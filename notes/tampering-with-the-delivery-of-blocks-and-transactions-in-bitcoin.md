# Tampering with the Delivery of Blocks and Transactions in Bitcoin
Arthur Gervais, Hubert Ritzdorf, Ghassan O. Karame and Srdjan Capkun
___


There are four scaling mechanims in Bitcoin that let it scale to ~7 tx/s:

1. Static timeouts when requesting blocks (20 mins) and txs (2 mins) from peers
2. Uses adveristment based protocol to decrease amount of traffic on wire (invs are small compared to block/tx messages)
3. For requesting TXs, client maintains a FIFO list of peers. If one peer times out (2mins), next peer is queries
4. Maintains a reputation system to penalize (and eventually disconnect from) bad peers

We can exploit how these mechanisms work to actually slow down delivery of blocks/tx to a client
___

### Requirements to slow down a peer:
1. Be the first one the advertise block/tx to victim. Since every verifies blocks/txs before advertising them, you can advertise faster by skipping verification (gets you an edge of 174ms on average)
2. Wait 20mins/2mins before actually sending the block/tx respectively

___

**Observation:** If attacker is significantly more well-connected to peers in Bitcoin network (especially gateways of pools) than victim, attacker will have sigificantly more chances of being the first one to advertise block/tx to a victim and, by extension, deny them the actual object

___

### Denying Blocks/Txs:
Once you've sent them an INV message for an object (Block/TX) they haven't heard of, they'll send you a GETDATA asking for that object. If you don't reply, they'll wait 20mins for blocks, 2 mins for txs before disconnecting from you.
Thus, you can slow down a node by 20/2 minutes.
___


### Denying Consecutive Blocks/Txs:
If you're quick (read first) to advertise (send INV), and consequently deny, an object you can deny the victim the object repeatedly (with decreasing probablity as number of denied blocks increase)

___

### Implications:
**Selfish mining**:
The mining power needed to attack network by selfish mining decreases if you're also denying blocks to the miners that learn about honest blocks

**0-confirmation double-spends:**

Different from typical 51% double-spend because it doesn't require any mining on part of the attacker.

**Td** = self-serving TX, **Tl** = payment to vendor

Attacker _denies_ Td to vendor while sends it to rest of the network for mining. Also sends Tl to vendor and, by extension, network. Vendor sees Tl and agrees that he has been paid.
All the while, attacker keeps denying Td to vendor (by repeatedly being the first one to send INV to vendor). Since the vendor doesn't detect double-spend and has Tl, he releases goods. Td gets mined by rest of the network and Tl gets dropped (because it spends outputs Td has confirmed spent).

The problem with this attack is that prevent-delivery has to happen for more than 10 mins (i.e. enough time for Td to get mined by rest of the network), which is hard since you can only prevent delivery for 2mins for txs and chances of repeat prevent delivery fall (only 20% for 10 consecutive delivery preventions that the attack a) mentions b) might actually require for mining).

**1-confirmation double spends:**
Sounds cute (you withhold blocks from victim _and_ miner). Probably won't work since miners are probably multihomed and since miners are only profitable if they work in pools, and pools are _always_ multihomed
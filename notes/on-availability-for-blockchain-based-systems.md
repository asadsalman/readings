# On Availability for Blockchain-Based Systems

Ingo Weber, Vincent Gramoli, Alex Ponomarev , Mark Staples, Ralph Holz, An Binh Tran, Paul Rimba
[Data61@CISRO, USydney, UNSW]


Looks into "Availablity" of a blockchain: transactions commit time and causes
Especially found network reordering to be a non-trival factor

Effects of gas price on block inclusion

Measurement Methodology:
1. Logs every incoming transaction
2. Look at incoming blocks and their content
3. Look at the blockchain retroactively

## Bitcoin:
Modified `btcd` to log every incoming tx
2 experiment that ran for 25hrs each

Talked about how much time it takes for transaction to commit (include + 6 confirmations)

Effects of tx fee and `locktime` on confirmation time

Found that txs arriving out of order (i.e. a tx arrives before its ancestors) too longer to commit

## Ethereum:
Logged every tx and block (_before_ verification, to log invalid ones too)
        recorded local time (since txs carry no timestamps of their own)

Observed that they did not get announcement for all txs that ended up being included in the blockchain, sometimes the tx announcement came after the including block

Uncle rate

Number of forks

Given the nature of tx commitment in Ethereum, a transaction might get included in an (uncle) block but never make it to the main chain if blocks afterward don't reference it

Time needed for commit

Effect of having high miner reward (gas) on inclusion/confirmation time

Block's Gas Limit: Block-specific limit of how much gas txs in the blocks can use (analogous to  block size in bitcoin)
        miner-defined variable, to some extent
Analyzed gas used by various transactions on the blockchain, found that almost all (except contract creations) finish unde rprothe miner-agreed-up block gas limit

described a mechanism to abort transactions (i.e. create another, higher fee, at same nonce i and wait for it to get accepted instead)
        experimentally evaluated its likelihood of success

Alexis' reivew of different consenus algorithms, WIP but useful:
https://github.com/Mechanism-Labs/MetaAnalysis-of-Alternative-Consensus-Protocols/blob/master/MetaAnalysis.pdf

___

![Throughput/Membership size of various consensus algorithms](consensus-plot.png)


Throughput/Membership size of various consensus algorithms, from: M. Vukolic: The Quest for Scalable Blockchain Fabric: Proof-of-Work vs. BFT Replication. Proc. iNetSec 2015



___

## read parts from: "SoK: A Consensus Taxonomy in the Blockchain Era"
	formal-ish approach to analyzing consensus and ledger algorithms
	talks about different bounds (timing, computational) and network synchrony models

## Inside Hyperledger:
	YAC:
		 https://arxiv.org/pdf/1809.00554.pdf
		 Blockchain consensus protocol (used inside Hyperledger)
		 byzantine fault tolerant
		 asynchronous

	
	 BFT-SMaRt:
		 thesis with the only description of the actual concrete consensus algorithm instantiated out of the described abstractions:
			 http://repositorio.ul.pt/bitstream/10451/35021/1/ulsd732239_td_Joao_Sousa.pdf
			 ^spoiler: uses pbft
		 A Guided Tour on the Theory and Practice of State Machine Replication
		 https://tclouds.schunter.org/downloads/factsheets/tclouds-factsheet-11-bftsmart.pdf
		 https://www.di.fc.ul.pt/~bessani/publications/dsn14-bftsmart.pdf
		 ^this paper more describes how bftsmart is great as a library, how modular, robust, fast it is but doesn't really talk much about the consensus algorithm
		 "From Byzantine Consensus to BFT State Machine Replication: A Latency-Optimal Transformation"
		 ^talks about the consensus algorithm itself
		 partial synchrony: "MOD-SMART does not require synchrony to assure safety. However, it requires synchrony to provide liveness."
		 a replica attaches timers to every message (i.e. transaction), if it the leader doesn't propose a message for two timeouts, replicas choose new leader (locally), _synchronization_ phase is started, this avoids censorship
		 BFT-smart, talks more about how to abstract and modularize consensus algorithms, as opposed to describing one specific consensus algorithm. Their techniques can be applied to "most paxos-based protocols", thus the applicable synchrony model (and timeouts) are the same as paxos-based protocols.


## XFT:

	thesis: http://www.eurecom.fr/fr/publication/4717/download/rs-publi-4717.pdf
	pseudo-code: https://arxiv.org/pdf/1502.05831.pdf
	"XFT allows designing reliable protocols that tolerate one class of machine faults (e.g., crash faults) regardless of the network faults (asynchrony)
		and that, at the same time, tolerate another type of machine faults (e.g., non-crash faults) only when the network is "synchronous enough""
	argues that byzantine model assumes really powerful adversary, and thus, is very restrictive
		by extension, BFT protocols are complex and require more processes in order to work n=3t+1, while crash-tolerant ones only require 2t+1
		by relaxing the byzantine model: i.e. assuming the adversary can't control machines _and_ network at the same time, we can lower the required number of non-byzantine processes, get cft _and_ BFT
	"In a nutshell, XFT models crash faults, non-crash faults, and network faults as independent events, which is very reasonable in practice"
	"but also tolerates Byzantine faults in combination with network asynchrony, as long as a majority of replicas are correct and communicate synchronously."
	"By practical evaluation on Amazon EC2 we show that, XPaxos maintains the similar performance as that of Paxos, and significantly outperforms two representative BFT protocols. "

	"A client that times out without committing the requests broadcasts the request to all active replicas. Active repli-cas then forward such a request to the primary and trig-ger a retransmission timer, within which a correct active replica expects the client’s request to be committed"
	only moves forward with a t+1 quorum called synchronous group, if progress isn't "timely", view change is used establish new group that _does_ move forward within time constraints
	note:
		- can we repeatedly force synchronous group view changes by not meeting timing requirements
		- how many group view changes would it require till in converges on an entirely non-byzantine synchronous group?
		- even when it converges on a non-byzantine group, can we simply flood/dos a member process to delay its process _just_ enough to cause another view change?
	
	two timeouts trigger view-change: retransmission timer and view-change timer (2delta, delta has to be _manually_ defined (or is a property of the network))
	view-change is unusual, not leader based, everybody participates
	^reason: "Note that XPaxos requires all active replicas in the new view to collect the most recent state and its proof (i.e., VIEW-CHANGE messages), rather than only the new primary. Otherwise, a faulty new primary could, even outside anarchy, purposely omit VIEW-CHANGE messages that contain the most recent state. Active replica sj in view i+1 waits for at least nt VIEW-CHANGE messages from all, but also waits for 2D time, trying to collect as many messages as possible"
	"a cooperative lazy replication mechanism in order to transfer the most recent state efficiently to replicas out of synchronous group"

## Algorand:

	ALGORAND AGREEMENT: https://eprint.iacr.org/2018/377.pdf
	Full-length paper: https://arxiv.org/pdf/1607.01341.pdf
	SOSP'17: https://people.csail.mit.edu/nickolai/papers/gilad-algorand-eprint.pdf
	
	"running on 1,000 Amazon EC2 VMs demonstrate that Algorand can confirm a 1 MByte block of transactions in ∼22 seconds with 50,000 users, that Algorand’s latency remains nearly constant when scaling to half a million users, that Algorand achieves 125× the transaction throughput of Bitcoin"

## Honey Badger BFT:
	series of blogposts by POA who are building honeybadger implementation seperately, really easy to understand articles:
		https://medium.com/poa-network/poa-network-building-honey-badger-bft-c953afa4d926
		https://medium.com/poa-network/poa-network-how-honey-badger-bft-consensus-works-4b16c0f1ff94
		https://medium.com/poa-network/poa-network-honey-badger-bft-and-threshold-cryptography-c43e10fadd87

	Short paper: https://www.zurich.ibm.com/dccl/papers/miller_dccl.pdf
	Great chapter by Cachin, describes reliable broadcast, common coin and Asynchronous Binary Byzantine agreement with algorithm (the only approachable description I could find): https://lpd.epfl.ch/site/_media/education/sdc_byzconsensus.pdf
	paper's arguments against partially synchronous protocols:
		- availability suffers in case of network troubles: which makes complete sense, if replicas can't talk to each other, they are not going to be commiting. Most systems still allow reads (read from t+1 is plenty in bft)
		- slow/unpredictable networks cause "significant degradation" in througput, which I feel is a much more legit argument, such protocols are frequently leader-based, and can be slow to recovery from leader crash or parition
	doesn't care about network/timing paramter tuning, while synchronous protocols strive to find good timeout values

	problem with (semi/weakly/partially) synchronous protocols:
		they have timing assumptions on how long it takes for, say, leader to response, how long they should wait for precommits, etc
		in case they haven't heard from leader in a while (or observed that leader is behaving badly e.g. censoring txs), they will elect new leader
		the time they wait for leader to respond can be large, especially if they've been adapting to bad network in the first place. Thus, if they end up choosing a bad (or crashed) leader during a period of good network (i.e. after GST), they will just wait (possibly) a long time before relecting as opposed to making fast progress during post-GST
		This. is. Bad.

	suggested usecases:
		- permissioned networksb
		- permissionless, but form committee off PoW (solves Sybil), commitee runs HoneyBadgerBFT

	**KEY POINT** While HBBFT does not require synchrony to reach consensus itself, it does use (semi?) synchronous protocol to generate threshold key shares. So, while it doesn't really require synchrony to run, HBBFT does need synchrony to bootstrap
	"(Trusted setup) For ease of presentation, we assume that nodes may interact with a trusted dealer during an initial protocol-specific setup phase, which we will use to establish public keys and secret shares. Note that in a real deployment, if an actual trusted party is unavailable, then a distributed key generation protocol could be used instead (c.f., Boldyreva [11]). All the distributed key generation protocols we know of rely on timing assumptions; fortunately these assumptions need only to hold during setup"

 

## Tendermint:

	https://hackernoon.com/tendermint-explained-bringing-bft-based-pos-to-the-public-blockchain-domain-8e5b0d360fa3
	tendermint works in wan setting
	high (hundreds) of node count
	nodes can be in different administrative bodies (i.e. non trusting)
	difference from pbft: - waits for 2f + 1 votes in stake not count
						  - gossip based, not all to all
	BLOCKS IF MORE THAN 1/3 VALIDATORS ARE OFFLINE OR PARTITIONED OFF
	proposer (i.e. leader) is picked deterministically, weighted using stake 
	uses timeouts for various decisions: to decide of new leader needs to be picked, waiting for prevotes/precommits
		timeouts are incremented in rounds (to match GST?)
		partial synchrony i.e. delta bounded after GST: "The last role is achieved by increasing the timeouts with every new round r, i.e, timeoutX(r) = initT imeoutX+r∗timeoutDelta; "
	"The beginning of each round has a weak dependence on synchrony as it utilizes local clocks to determine when to skip a proposer. That is, if a val-idator does not receive a proposal within a locally measured TimeoutPropose of entering a new round, it can vote to skip the proposer. Inherent in this mechanism is a weak synchrony assumption, namely, that the proposal will eventually be delivered within TimeoutPropose, which may itself increment with each round. This assumption is discussed more fully in Chapter 10."
	"After the proposal, rounds proceed in a fully asynchronous manner - a validator makes progress only after hearing from at least two-thirds of the other validators. This relieves any sort of dependence on synchronized clocks or bounded network delays, but implies that the network will halt if one-third or more of the validators become unresponsive. This circuit of weakly synchronous proposals, followed by asynchronous voting, is depicted in Figure 3.1"
	*Question: can we always cause a round to fail by dropping/delaying 1/3 proposals, prevotes, precommits?*

	Tendermint thesis and paper give contradictory information:
		thesis claims that only timeout is for proposal (on timeout you go to next round: new proposer), everything else (prevote, precommit) is async i.e. blocks until you get 2/3 votes
		paper claims all three phases (proposal, prevote, precommit) have a timeout as to _avoid_ blocking (but the timeouts are adapting i.e. converge to GST)
		Code: uses timeouts for all

	"However, in contrast to synchronous systems, the speed of progress does not depend on system parameters, but instead depends on real network speed."
	uses gossip to propagate messages to all processes, signed messages

	question: since tendermint increases timeout when starting a new round when the last round failed, it can eventually have very large timeouts, essentially becoming asynchronous, right?

## Bitcoin:

	is synchronous (delta = 10 min)
	"Let’s explore the synchronous case, using a well-known protocol for reference: the Bitcoin protocol. In Bitcoin, there is a “known fixed upper bound”. This is referring to the 10 minute block time. In order for the Bitcoin network to move forward with creating blocks, the protocol artificially imposes a timing assumption, giving the entire network of nodes 10 whole minutes to listen for, collect, validate, and gossip transactions propagating across its peer network."

## Ethereum:

	http://gramoli.redbellyblockchain.io/web/doc/pubs/ethereum-mitm-attacks.pdf

## Red Belly Blockchain
	https://arxiv.org/pdf/1812.11747.pdf
	http://csrg.redbellyblockchain.io/doc/ConsensusRedBellyBlockchain.pdf
	RBBC major goal: "Democratic" BFT, instead of one leader, propose "superblock" using contribution of multiple proposers
	idea of community blockchain: permissioned nodes run the verification (in shards!) but anyone (read: permissionless) can issue txs
	paper builds a safe, non-terminating binary consensus algorithm -> builds terminating (i.e. timeout-based) safe binary consensus algorithm -> builds terminating, safe multivalued (i.e. blocks) consensus scheme with "weak" leader
	funny assumption: no signatures required, instead requires full mesh	BAMP(t/3, Synch)
	description of how to build multi-valued byz consensus off binary consensus (section 4): https://research.csiro.au/data61/wp-content/uploads/sites/85/2016/08/byz-block.pdf
	^same people, same algorithm as paper, but found this description more approachable
	another, simple, approach of building multivalued consensus on top of binary agreement: https://groups.csail.mit.edu/tds/papers/Coan/TurpinCoan-ipl84.pdf

## One direction we can take with research:
	[a13-clement] talks about "dark secret of bft", and how protocols optimize for the "gracious" case of their protocol, whatever that is.
	And even with non-exotic (i.e. non-all-powerful byzantine behavior) attacks happen, they suffer tremendously (in throughput for example), it argues that while they _survive_ byzantine behavior, they can not tolerate it
	^also gives examples where only a simple malformed MAC or slow primary (but not slow enough to trigger view change, maybe?) can cause serious degradation (Yair's work too)
	one direction I would like to explore would be such non-exotic cases that cause serious degradation in throughput 
	[a13-clement] http://www.cs.cornell.edu/lorenzo/papers/a13-clement.pdf
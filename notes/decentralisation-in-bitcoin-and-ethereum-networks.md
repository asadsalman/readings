# Decentralisation in Bitcoin and Ethereum networks

Adem Efe Gencer, Soumya Basu, Ittay Eyal, Robbert van Renesse, Emin Gün Sirer

___
# To reach 90% of the mining power needs just 16 miners in Bitcoin, and 11 in Ethereum.
___

## Infrastructure
experiments of varying duration, days till 12 months
continously collected data about provisioned bandwidth and latency
used 18 vantage points, 15 of which are on PlanetLab: 3 hosted at Cornell (because of resource requirement of some measurement)
seeded measurement of nodes (didn't do crawling themselves) from crawling sites and locally deployed "supernode"

limitations of measurements:
- can't measure nodes behind NATs
- can't measure mining pool participants that _only_ talk to gateway nodes (can measure gateway, though)
___

## Provisioned bandwidth
Provisioned bandwidth is an estimate on a node’s transmission capacity charac-terizing how much bandwidth the node has to communicate with the rest of the cryptocurrency network

"BMS measures the provisioned bandwidth of each peer by re-questing a large amount of data from each peer and seeing how fast the peers can stream the data to BMS’s measurement nodes"
^they're requesting multiple block (i.e. many short-lived connections) resonable measures of "bandwidth"
answer: probably no for ground-truth (since short-lived connections die before network can converge on actual bandwidth value), but probably good enough for bitcoin/ethereum purpose since actual (non-measurement) connections are going to be short-lived too
^they do eliminate streams shorter than 500ms

Summary of results:

![bandwidth cdf](bandwidth.png =250px)

___

questions I have: ethics of measuring
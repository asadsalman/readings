# Hijacking Bitcoin: Routing Attacks on Cryptocurrencies
* Maria Apostolaki
* Aviv Zohar
* Laurent Vanbever


### Two properties make Bitcoin Network especially susceptible to underlying networking routing level attacks
1. High degree of centralization of mining power at AS level
2. How easy it is to hijack BGP routes (IP prefixes)

### Paper describes two kinds of attacks
1. **Partition Attack**: _Completely_ disconnect some nodes from rest of the network
2. **Delay Attack**: Drop/modify certain messages (GETDATA/BLOCK) to make requesting peers wait for 20mins. 
While partition attack requires _all_ connections of partition to outside be hijacked, delay attack can still be effective even when only some percentage of peers are hijacked (they give numbers in VIII).

### Practicality:
* 90 seconds to hijack routes and divert traffic through attacker AS
* hijacking of 50% peer connections leads to victim not learning of new blocks 60% of the time (as compared to a reference node)

### Impact: 
* Wasted mining -> wasted money
* Potential Censorship
* Double Spending

### How to attack:
As an ISP/AS, advertise a IP prefix that covers your victim IP addresses. There's a limit to how specific a prefix you can advertise (most AS discard advertisements more specific than /24). But authors found that 93% bitcoin nodes lie in a prefix shorter than /24, thus they're vulnerable.

### Partition Attack:
In order to create a partition where you want to isolate P (set of node victim IPs)
```
P = {victim IPs}
L = {} //nodes that have stealth connections with nodes outside the partition
U = {} //nodes inside the partition who haven't sent messages out
for pkt in hijacked_bitcoin_packet_stream:
	if P.contains(pkt.src):
		if L.contains(pkt.src):
			// packet was from inside the partition (from a different AS, though)
			// but is from a node identified to be leaky
			drop(pkt)
		else:
			last_seen[pkt.dst] = now()
			U.remove(pkt.ip_src)
			detect_leakage(pkt)
	else
		//packet was from outside the partition
		drop(pkt)
	for ip in P:
		if last_seen[ip] < now() - threshold:
			U.add(ip)
	if P == L.union(U):
		return;
```

```
detect_leakage(pkt):
	if type(pkt) == Block or type(pkt) == INV:
		//if the hash of the tx/block has been seen outside the partition
		if hash(pkt) in Blocks(~(P \ L)):
			L.add(pkt.src)
			drop(pkt)
```

### Delay Attack:
[illustration](delayattack.png)

If **victim -> attacker -> peer** (i.e. attacker is on path for outgoing messages to peer):
>attacker modifies victim's outgoing GETDATA to request a block older than the one in the INV message, the peer will send the older block to victim who will then discard it. The victim won't even disconnect from that peer. Wait for 20mins for that peer to send the right block.
>If the attacker can get the right block to victim within the 20mins timeout, the victim won't drop connection with the peer with attacker on path.

If **victim <- attacker <- peer** (i.e. attacker is on path for incoming messages to peer):
> attacker modifies a peer's outgoing data message (BLOCK) to victim by screwing up its checksum. They verified that the victim will wait 20mins (from the time it initially requested block) to disconnect with this peer
## Thoughts:
### Partition Algorithm:
1. ASes are all or none (IV-B) in the partitioning of their nodes. So if an AS has just one node that's not vulnerable, that whole AS is non attackable.
2. Someone talking on any port other than 8333 can go still talk to nodes outside (and inside) AS
3. While removing a leaking node from maximal feasible subset, you might hear a block advertisement from a non-stealthy-peering node that heard the outside block from a stealthy-peering node through intra-AS communication. You might then remove the non-stealthy-peering node from the maximal feasible subset.

The authors claim that the intra-node communication is small (1.14%). But:

1. Don't explain how they got this figure (only that they inferred it)
2. the effectiveness of whole attack is depending on this figure. The % of a node's intra-AS connection doesn't even need to be high for blocks to get gossiped through the AS (just one intra-AS peer would do).

Even when they're dropping packets from leakage nodes, the only packets the attacker can even touch is the one that passes their AS.

## Ideas I can use:
1. How they set up their infrastructure to test effects of partition attack (install specific drop rules on switches instead of dropping whole link)
2. maybe use scapy for proxy too
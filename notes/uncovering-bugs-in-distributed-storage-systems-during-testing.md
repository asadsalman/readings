# Uncovering Bugs in Distributed Storage Systems during Testing (Not in Production!)
USENIX FAST 2016

## Main idea:
Distributed systems are tough to test because of the non-determinism in the environment they run in.

There are various existing techniques:

1. TLA+ from MS: really good but this formal verification is done on specification and will definitely miss out on bugs that arise from actual implementation bugs

2. Model checkers: run unmodified binaries, also great but too many states to try, unfeasible


Another problem with existing testing systems is that they require code to be written in different, often domain-specific, languages that developers/testers are not used to.


## Their Contribution:
-System for modeling a distributed system, specifying desirable properties and a driver that systematically tests these properties.

-A language, P#, that can be hooked into systems written in C# to act like a middleware for the testing
___
## What the developer needs to do to use this system:

1. Replace actual networking calls in code with P# pseudo-networking calls (i.e. introduce their networking middleware which tracks/permutes messages passed)
2. Write test _harness_ that drives the system to states they want to investigate
3. Criteria for correction, i.e. assertions for safety and liveness of the system

First two are required so that system can introduce a degree of non-determinism into the system.

___

Once developer has provided environment for P# to drive tests in, P# explores possible states by introducing non-determinism in the input (I imagine the non-determinism can be bound/controlled in some way). Some ways include: delaying packets, dropping packets, sending them out of order, killing random nodes etc. The system keeps exploring the "state tree" until user-specified limits like time or number of executions (or until it finds a bug).

If it finds a bug, because it's tracking all states that go in and out of a system with its networking middleware, it provides globally-ordered trace to developer to reproduce/test with.

___
## How to model a system:
1. Use existing components: Write wrappers in P# to direct non-deterministic calls (file reads/networks) to the system. This done so the system can now control all non-deterministic elements of the system and can permute through them to break the system (drop/delay packets etc)
2. Create a simple state machine out of more simpler systems. This way you don't have to deal with the whole system, only relevant you want to test. Even here, you can reuse components of actual code as API calls too (has to be C# though)

___
## Separating timing:
The correctness of a system shouldn't depend on individual timer at each host, thus disables clocks kept by individual components, instead replaces them with clock from P# API. This way P# can, again, induce timing-related faults systematically. 

___
## Safety monitor
You can write "assertions" that need to hold true for the system. These assertions can be local ones or global ones. If an assertion fails, you can get trace of messages that caused the assertion to fail.

___
## Liveness monitor:
**Hot state:** You just made a blocking call (set request, for example)

**Cold state:** Blocking call resolved (got reply)

If a component is in hot state for "infinitely" long time, i.e. user-defined timeout or length of experiment, then component violates liveness.

___

**Thoughts**:

* While they do mention state-space explosion as one of the problems with other systems, they don't describe how _they_ deal with it.
* The central timing system could be huge pitfall if it provides a consistent global clock to all components (i.e. something not possible in actual distributed systems), but, if they're doing it right, it shouldn't be too hard to provide different clocks to different components to fake skew etc
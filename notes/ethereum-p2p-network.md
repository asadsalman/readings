# MEASURING ETHEREUM’S PEER-TO-PEER NETWORK

## The Paper:
___
a. 82-day "view of the Ethereum ecosystem"
b. 24-hr snapshot view of peers on the main Ethereum network (size, latency, geo distribution, node freshness)

they measured:
	- time to peer-limit

	- number/type of messages sent out

	- reason of disconnect

	- periodcally logged devp2p HELLO and Ethereum STATUS

	- connection latency, by obtaining the smoothed RTT of connection from TCP socket

	- different services, genesis hash

	- variety of clients (and versions!)

	- node freshness (how up to date to the blockchain are they)


```
From Ethereum STATUS messages received from 323,584 nodes, we found a wide and substantial range of 4,076 networks and 18,829 genesis hashes.
```

they're checking for ethereum classic by sending `GET_BLOCK_HEADERS`, querying for the DAO fork block (and disconnect immediately afterwards)

48% of p2p network isn't mainstream Ethereum (non-classic)

validation technique:
	were able to discover own (30) nodes 
```
"NodeFinder dis-connects from peers and frees up their peer slots as soon as it is done collecting information from peer connection establishment, which consists of DEVp2p handshake, Ethereum handshake, and DAO fork block verification"
```
but!
```
"NodeFinder then periodically reconnects to known nodes to track longitudinal properties such as liveliness and churn"
```

### connection with only 11.8% of the RLPx discovered peers went through
(for us, this ratio was like somewhere less than 50% but we discovered much less peers than them)

what we could look at:

	- if/why parity tends to connect with parity clients?

___
## The Thesis:
___
five month study
study of all networks, genesis hashes, and forks in the ethereum universe

client maintained at least 2,000 peer connections

```
"We implement our scanning utility and peering client based on Geth. [...] Our choice is based on the assumption that Geth’s protocol implementation is compatible with majority of the network"
```
^ we found that there were **serious** deviations in how protocols are implemented

```
"The discovery client’s database provided about 10 thousand node addresses for each cycle, and each scan against the 10 thousand nodes finished within 30 seconds. Considering the data processing time and occassional delays, we allotted 4 minutes to each cycle"
```

```
"To summarize our findings from the Phase 1 results, out of the most recently seen 10 thousand nodes reported by the discovery client, more than 3 thousands are verified to be publicly-reachable active DEVp2p nodes; however, more than half are not available for peer connections due to having too many peers."
```

they queried for randomly generated ids in FindNode

also, once their network id had "sufficently" propogated through the network, they started receiving more and more incoming connections which is great since all incoming connections were from currently active node, and you learn about nodes that aren't publicly reachable

open questions we can answer:

	look at the gnutella, napster, overnet, bittorrent studies cited in the paper and see if they studied parameters (population, bottleneck bandwidth, latencies, availability, churn, degree of connections) that we can look into

	they didn't look into fast synch, maybe look into that

	mempool (pending) study? log every incoming unconfirmed tx, answer bunch of interesting questions

	centralization

what they did:

	- client type distribution

	- lantencies (and, by extension, stucture)

	- connection length i.e. churn

	- physical location distribution

	- information propogation:

		- number fo txs sent by peers

		- where (peer count) txs are coming from 

		- how far behind in synching are peers


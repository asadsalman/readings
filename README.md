## Papers I'm reading and notes on them:
1. [Hijacking Bitcoin: Routing Attacks on Cryptocurrencies](notes/hijacking-bitcoin-routing-attacks-on-cryptocurrencies.md)
2. [Tampering with the Delivery of Blocks and Transactions in Bitcoin](notes/tampering-with-the-delivery-of-blocks-and-transactions-in-bitcoin.md)
3. [Uncovering Bugs in Distributed Storage Systems during Testing (Not in Production!)](notes/uncovering-bugs-in-distributed-storage-systems-during-testing.md)
4. [A whole bunch of consensus systems, notes are specifically from the point-of-view of network synchrony but do include other details](notes/synchrony-of-blockchain-systems.md). Systems include:
	* Inside Hyperledger:
		* YAC
		* BFT-SMaRt
	* XFT
	* Algorand
	* Honeybadger BFT
	* Tendermint
	* Bitcoin
	* Ethereum
	* Red Belly Blockchain
	
5. [Measuring Ethereum Network Peers](notes/ethereum-p2p-network.md)
6. [Decentralization in Bitcoin and Ethereum Networks](notes/decentralisation-in-bitcoin-and-ethereum-networks.md)
7. [On Availability for Blockchain-Based Systems](notes/on-availability-for-blockchain-based-systems.md)